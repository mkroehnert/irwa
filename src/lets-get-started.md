# Let's Get Started

## Pre-Requisite Packages (Debian-based Linux)

In order to install Rustup using the instructions on the website, you will need
curl:

```sh
$ sudo apt update
$ sudo apt install curl
```

In order to run the compiler and other tools, we need some basic development
tools installed:

```sh
$ sudo apt install build-essential
```

## Installation on Windows

(**NOTE:** I haven't tested these instructions; they were contributed by
reader Miodrag Milić. I don't run Windows, but server stats show that a lot
of you do, so I wanted to add these instructions in the hope they are
helpful. If you have problems, please [open an
issue](https://gitlab.com/bstpierre/irwa/issues).)

Windows users should have [chocolatey](https://chocolatey.org/): `iwr
https://chocolatey.org/install.ps1 | iex`.

Subsequent opreations are run in the admin shell.

### Install general prereqs

C++ tools, rustup, Rust

```
cinst -y visualstudio2017-workload-vctools
iwr https://win.rustup.rs/x86_64 -outf rustup-init.exe
./rustup-init -y
refreshenv
```

### Install project prereqs

SQLite3

```
cd ...\mytodo

cinst -y sqlite
sal lib "${Env:ProgramFiles(x86)}\Microsoft Visual
Studio\2017\BuildTools\VC\Tools\MSVC\14.11.25503\bin\HostX64\x64\lib.exe"
lib /def: $env:ChocolateyInstall\lib\SQLite\tools\sqlite3.def  /machine:X64
/out:sqlite3.lib
$Env:SQLITE3_LIB_DIR = $pwd
cargo install diesel_cli --no-default-features --features sqlite

cp $env:ChocolateyInstall\lib\SQLite\tools\sqlite3.dll . # diesel.exe requires it

'DATABASE_URL=./testdb.sqlite3' | Out-File .env  #doesn't seem to work, use
next line
$Env:DATABASE_URL = "$pwd/testdb.sqlite3"
diesel setup
```

## Install Rust via rustup

Go to [https://rustup.rs](https://rustup.rs) and follow the instructions. Note that some linux
distributions provide stable Rust releases, but for the purposes of what
we're doing here we want to use a nightly toolchain.

Install the nightly toolchain: `rustup toolchain install nightly`.

And then set it to be the global default: `rustup default nightly`.

## Generate Our App

We can verify that our installation was successful and generate the
skeleton for our app:

```sh
$ cargo new mytodo
$ cd mytodo
$ cargo run
```

You should see messages as your app builds, and then the "Hello World"
greeting. If not, go back to [https://rustup.rs](https://rustup.rs) and verify that you've
followed directions correctly.
