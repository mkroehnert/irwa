#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
// ANCHOR: use
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde;
// ANCHOR_END: use

// ANCHOR: json
use rocket_contrib::json::Json;
// ANCHOR_END: json

use mytodo::db::{query_task, establish_connection};
use mytodo::db::models::Task;

// ANCHOR: response
#[derive(Serialize)]
struct JsonApiResponse {
    data: Vec<Task>,
}
// ANCHOR_END: response

// ANCHOR: tasks_get
#[get("/tasks")]
fn tasks_get() -> Json<JsonApiResponse> {
    let mut response = JsonApiResponse { data: vec![], };

    let conn = establish_connection();
    for task in query_task(&conn) {
        response.data.push(task);
    }

    Json(response)
}
// ANCHOR_END: tasks_get

fn main() {
    rocket::ignite()
        .mount("/", routes![tasks_get])
        .launch();
}
